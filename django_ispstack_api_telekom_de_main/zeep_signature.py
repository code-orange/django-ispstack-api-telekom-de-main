from zeep.wsse import BinarySignature


class BinarySignatureNoVerify(BinarySignature):
    def verify(self, envelope):
        return envelope
