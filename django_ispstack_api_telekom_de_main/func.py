def format_tk_de_phone_number(phone_number):
    num_split = phone_number.as_international.split(" ")
    num_fmt = num_split[0] + " "

    for num_part in num_split[1:]:
        num_fmt += num_part

    return num_fmt
